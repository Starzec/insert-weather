import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';
import {CurrentWeather} from '../models/currentWeather';
import {WeatherService} from '../services/weather.service';
import {FrontWeatherData} from '../models/frontWeatherData';
import {AdditionalInformation} from '../models/additionalInformation';

@Component({
  selector: 'app-weather-panel',
  templateUrl: './weather-panel.component.html',
  styleUrls: ['./weather-panel.component.css']
})
export class WeatherPanelComponent implements OnInit, OnDestroy {

  @Input() currentBackground: string;
  updateWeatherInterval: Subscription;
  currentWeather: CurrentWeather;
  lastUpdate: string;
  frontWeatherData: FrontWeatherData;
  additionalInformation: AdditionalInformation;

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.updateWeatherInterval = interval(5 * 60 * 1000).subscribe(_ => this.onWeatherAsk());
    this.weatherService.currentWeatherChanged.subscribe(weather => {
      this.currentWeather = weather
      this.frontWeatherData = FrontWeatherData.fromCurrentWeather(this.currentWeather, this.lastUpdate);
      this.additionalInformation = AdditionalInformation.fromCurrentWeather(this.currentWeather);
    });
    this.onWeatherAsk();
  }

  ngOnDestroy(): void {
    this.updateWeatherInterval.unsubscribe();
    this.weatherService.currentWeatherChanged.unsubscribe();
  }

  onWeatherAsk() {
    this.weatherService.takeCurrentWeather();
    this.lastUpdate = new Date().toLocaleString();
  }
}
