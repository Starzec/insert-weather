import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FrontWeatherData} from '../../models/frontWeatherData';

@Component({
  selector: 'app-front-information',
  templateUrl: './front-information.component.html',
  styleUrls: ['./front-information.component.css']
})
export class FrontInformationComponent implements OnInit {

  @Input() frontInformation: FrontWeatherData;
  @Output() updateWeather = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onUpdateWeather(): void {
    this.updateWeather.emit();
  }
}
