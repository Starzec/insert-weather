import {Component, Input, OnInit} from '@angular/core';
import {AdditionalInformation} from '../../models/additionalInformation';

@Component({
  selector: 'app-additional-information',
  templateUrl: './additional-information.component.html',
  styleUrls: ['./additional-information.component.css']
})
export class AdditionalInformationComponent implements OnInit {

  @Input() additionalInfo: AdditionalInformation;

  constructor() { }

  ngOnInit(): void {
  }

}
