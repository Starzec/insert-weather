import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CurrentWeather, CurrentWeatherBuilder} from '../models/currentWeather';
import {Subject} from 'rxjs';


const WEATHER_URL: string = 'https://api.openweathermap.org/data/2.5/weather';
const CITY_ID: string = '3081368';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  currentWeather: CurrentWeather;
  currentWeatherChanged = new Subject<CurrentWeather>();

  constructor(private http: HttpClient){}

  takeCurrentWeather(units: string = 'metric') {
    this.http.get(WEATHER_URL, {
      params: {
        id: CITY_ID,
        appid: environment.apiKey,
        units: units
      }
    }).subscribe(
      (response: any) => {
        this.currentWeather = new CurrentWeatherBuilder()
          .condition(response.weather[0].main)
          .description(response.weather[0].description)
          .iconId(response.weather[0].icon)
          .temperature(response.main.temp)
          .feelsLike(response.main.feels_like)
          .tempMin(response.main.temp_min)
          .tempMax(response.main.temp_max)
          .pressure(response.main.pressure)
          .humidity(response.main.humidity)
          .cloudiness(response.clouds.all)
          .build();
        this.currentWeatherChanged.next(this.currentWeather);
      }
    );
  }
}
