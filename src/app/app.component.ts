import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Insert';

  currentBackgroundInterval: Subscription;
  currentBackground: string;

  constructor() {}

  ngOnInit(): void {
    this.currentBackgroundInterval = interval(1000).subscribe(_ => {
      let currentHour = new Date().getHours();
      if(this.currentBackground != 'midday' && currentHour >= 12 && currentHour < 20) {
        this.currentBackground = 'midday';
      } else if(this.currentBackground != 'morning' && currentHour >= 4 && currentHour < 12) {
        this.currentBackground = 'morning';
      } else if(this.currentBackground != 'night' && (currentHour >= 20 && currentHour < 24 || currentHour >= 0 && currentHour < 4)) {
        this.currentBackground = 'night';
      }
    });
  }

  ngOnDestroy(): void {
    this.currentBackgroundInterval.unsubscribe();
  }
}
