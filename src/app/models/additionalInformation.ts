import {CurrentWeather} from './currentWeather';

export class AdditionalInformation {
  constructor(public feelsLike: number,
              public tempMin: number,
              public tempMax: number,
              public pressure: number,
              public humidity: number,
              public cloudiness: number) {
  }

  static fromCurrentWeather(currentWeather: CurrentWeather): AdditionalInformation {
    return new AdditionalInformation(currentWeather.feelsLike,
      currentWeather.tempMin,
      currentWeather.tempMax,
      currentWeather.pressure,
      currentWeather.humidity,
      currentWeather.cloudiness);
  }
}
