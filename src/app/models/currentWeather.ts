import {FrontWeatherData} from './frontWeatherData';

export class CurrentWeather {
  constructor(public condition: string,
              public description: string,
              public iconId: string,
              public temperature: number,
              public feelsLike: number,
              public tempMin: number,
              public tempMax: number,
              public pressure: number,
              public humidity: number,
              public cloudiness: number) {}
}

export class CurrentWeatherBuilder {
  private readonly currentWeather: CurrentWeather;

  constructor() {
    this.currentWeather = {
      condition: '',
      description: '',
      iconId: '',
      temperature: 0,
      feelsLike: 0,
      tempMin: 0,
      tempMax: 0,
      pressure: 0,
      humidity: 0,
      cloudiness: 0
    };
  }

  condition(condition: string): CurrentWeatherBuilder {
    this.currentWeather.condition = condition;
    return this;
  }

  description(description: string): CurrentWeatherBuilder {
    this.currentWeather.description = description;
    return this;
  }

  iconId(iconId: string): CurrentWeatherBuilder {
    this.currentWeather.iconId = iconId;
    return this;
  }

  temperature(temperature: number): CurrentWeatherBuilder {
    this.currentWeather.temperature = temperature;
    return this;
  }

  feelsLike(feelsLike: number): CurrentWeatherBuilder {
    this.currentWeather.feelsLike = feelsLike;
    return this;
  }

  tempMin(tempMin: number): CurrentWeatherBuilder {
    this.currentWeather.tempMin = tempMin;
    return this;
  }

  tempMax(tempMax: number): CurrentWeatherBuilder {
    this.currentWeather.tempMax = tempMax;
    return this;
  }

  pressure(pressure: number): CurrentWeatherBuilder {
    this.currentWeather.pressure = pressure;
    return this;
  }

  humidity(humidity: number): CurrentWeatherBuilder {
    this.currentWeather.humidity = humidity;
    return this;
  }

  cloudiness(cloudiness: number): CurrentWeatherBuilder {
    this.currentWeather.cloudiness = cloudiness;
    return this;
  }

  build(): CurrentWeather {
    return this.currentWeather;
  }
}
