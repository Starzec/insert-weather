import {CurrentWeather} from './currentWeather';

export class FrontWeatherData {
  constructor(public condition: string,
              public description: string,
              public iconId: string,
              public temperature: number,
              public lastUpdate: string) {
  }

  static fromCurrentWeather(currentWeather: CurrentWeather, lastUpdate: string) {
    return new FrontWeatherData(currentWeather.condition, currentWeather.description, currentWeather.iconId, currentWeather.temperature, lastUpdate);
  }
}
