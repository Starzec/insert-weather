import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { WeatherPanelComponent } from './weather-panel/weather-panel.component';
import { FrontInformationComponent } from './weather-panel/front-information/front-information.component';
import { AdditionalInformationComponent } from './weather-panel/additional-information/additional-information.component';

@NgModule({
  declarations: [
    AppComponent,
    WeatherPanelComponent,
    FrontInformationComponent,
    AdditionalInformationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
