const express = require('express');

const app = express();

app.use(express.static('./dist/Insert'));

app.get('/*', (req, res) =>
  res.sendFile('index.html', {root: 'dist/Insert/'}),
);

app.listen(process.env.PORT || 4200);
